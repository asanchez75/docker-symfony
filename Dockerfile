FROM centos:7
MAINTAINER Adam Sanchez <a.sanchez75@gmail.com>

#RUN yum install -y epel-release # for nginx
# Add IUS repository for PHP and update
RUN yum localinstall -y https://centos7.iuscommunity.org/ius-release.rpm && yum update -y
RUN yum install -y initscripts  # for old "service"
# named (dns server) service
#RUN yum install -y bind bind-utils
#RUN systemctl enable named.service


RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;


#install apache
RUN yum -y install httpd
RUN systemctl enable httpd.service

# Install PHP and modules
RUN yum install -y mod_php71u php71u-mysqlnd php71u-cli php71u-fpm php71u-common
RUN yum install -y php71u-mcrypt php71u-ldap php71u-mbstring \
                   php71u-pdo php71u-pgsql php71u-gd \
                   php71u-intl php71u-bcmath php71u-process  \
                   php71u-tidy php71u-xml php71u-xmlrpc \
                   php71u-devel php71u-pspell php71u-recode php71u-opcache php71u-json \
                   git curl vim node npm make automake epel-release
# Not yet supported php70u-pear php70u-pecl-apcu php70u-pecl-xdebug php70u-pecl-apcu \

# Add postgres client
#RUN sed -i 's/RPM-GPG-KEY-CentOS-7/RPM-GPG-KEY-CentOS-7\nexclude=postgresql*/g' /etc/yum.repos.d/CentOS-Base.repo && \
#    yum localinstall -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm && yum update -y && \
#    yum install -y postgresql96

# Add MariaDB client support
COPY MariaDB.repo /etc/yum.repos.d/MariaDB.repo

RUN yum update -y && yum install -y MariaDB-client

# Install utilities (xdebug is used by CI and Dev
RUN curl -o /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-5.7.3.phar && chmod +x /usr/local/bin/phpunit
RUN cd /usr/local/bin && curl -sS https://getcomposer.org/installer | php -- --filename=composer

# Drush
RUN cd /usr/local/src && git clone https://github.com/drush-ops/drush.git \
      && cd /usr/local/src/drush \
      && git checkout -b 8.x origin/8.x \
      && composer install

RUN ln -s /usr/local/src/drush/drush /usr/local/bin/drush

# Container configuration
WORKDIR /var/www/html

EXPOSE 9000

VOLUME /run /tmp

ADD cmd.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/cmd.sh

CMD ["/usr/local/bin/cmd.sh"]


